# Description

Identification of digits from MNIST dataset.
MNIST ("Modified National Institute of Standards and Technology") is the classic dataset of handwritten images.

# Content

- Importing data/python packages
- Preparing data
- Building CNN model
- Preparing submission for Kaggle competition
